'use strict'

var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var filter = require('gulp-filter');
var mainBowerFiles = require('main-bower-files');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var runSequence = require('run-sequence');
var replace = require('gulp-replace');

gulp.paths = {
    dist: 'public',
};

var paths = gulp.paths;


// Static Server without watching scss files


gulp.task('sass', function () {
    return gulp.src('./resources/assets/sass/style.scss')
        .pipe(sass())
        .pipe(gulp.dest('./public/css'))
        .pipe(browserSync.stream());
});
gulp.task('copy:js', function() {
    return gulp.src('./resources/assets/js/**/*')
        .pipe(concat('app.js'))
        .pipe(gulp.dest(paths.dist+'/js'));
});


gulp.task('sass:watch', function () {
    gulp.watch('./resources/assets/sass/**/*.scss');
});

gulp.task('build:dist', function(callback) {
    runSequence('clean:dist', 'copy:bower', 'copy:css', 'copy:img', 'copy:fonts', 'copy:js', 'copy:views', 'copy:html', 'replace:bower', callback);
});

gulp.task('default', ['sass']);

