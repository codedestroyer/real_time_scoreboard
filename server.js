var app = require('http').createServer(handler);
var io = require('socket.io')(app);

var Redis = require('ioredis');
var redis = new Redis();

app.listen(6001, function() {
    console.log('Server is running!');
});

function handler(req, res) {
    res.writeHead(200);
    res.end('');
}

io.on('connection', function(socket) {
    socket.on("change room",function(room){
        console.log("reconnect");
        socket.join(room);
});
    socket.on('foul', function(room) {
        console.log("Someone broke a rule ");
    });
    socket.on('newScoreToBeat', function(data) {
        socket.to(data.room).emit('updateLowest',data.score);
    });

    socket.on('scoreboardChange', function(data) {
        socket.to(data.room).emit('changeView',data.type);
    });


    socket.on('updateTime', function(data) {
        socket.to(data.room).emit('updateTime',data);
    });
    socket.on('updatePlayers', function(data) {
        socket.to(data.room).emit('updatePlayers',data);
    });
    socket.on('timeStarted', function(data) {
        console.log("sfsdf");
        socket.to(data.room).emit('timeStarted',data);
    });
});

redis.psubscribe('*', function(err, count) {

});

redis.on('pmessage', function(subscribed, channel, message) {
    message = JSON.parse(message);
    console.log(message.data.location + " " + message.event + " " + message.data);
    io.to(message.data.location).emit(message.event, message.data);
});
