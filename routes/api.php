<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::resource('location', 'LocationController');
Route::post('/game/score', 'GameController@storeScore');
Route::post('/game/round', 'GameController@storeRound');
Route::post('/game/addTotal', 'GameController@addScoreTotal');
Route::patch('/game/score', 'GameController@updateScore');
Route::get('game/list', 'GameController@getScores');
Route::resource('game', 'GameController');
