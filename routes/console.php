<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
  $new = new \App\Repositories\ScoreRepo(new \App\Models\Score());
  $totals = $new->getTotals(1,3);
  dd($totals);

})->describe('Display an inspiring quote');
