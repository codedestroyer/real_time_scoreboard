<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ScoreUpdated implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $gameId;
    public $location;
    public $score;
    public $index;

    public function __construct($data)
    {
        $this->gameId = $data['gameId'];
        $this->location = $data['location'];
        $this->score = $data['score'];
        $this->index = $data['index'];
    }

    public function broadcastOn()
    {
        return ['edsfuncade'];
    }

    /**
     * Get the broadcast event name.
     *
     * @return string
     */
    public function broadcastAs()
    {
        return 'update.score';
    }
}
