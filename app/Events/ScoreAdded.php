<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ScoreAdded implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $gameId;
    public $location;
    public $firstName;
    public $lastName;
    public $score;
    public $uniqueKey;

    public function __construct($data)
    {
        $this->gameId    = $data['gameId'];
        $this->location =  $data['location'];
        $this->firstName = $data['firstName'];
        $this->lastName  = $data['lastName'];
        $this->score     = $data['score'];
        $this->uniqueKey = $data['key'];
    }

    public function broadcastOn()
    {
        return ['edsfuncade'];
    }

    /**
     * Get the broadcast event name.
     *
     * @return string
     */
    public function broadcastAs()
    {
        return 'new.score';
    }


}
