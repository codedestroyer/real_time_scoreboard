<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class RoundAdded implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $gameId;
    public $location;
    public $key;
    public $lowestScore;
    public $totalScore;
    public $index;

    public function __construct($data)
    {
        $this->gameId = $data['gameId'];
        $this->location = $data['location'];
        $this->key = $data['key'];
        $this->lowestScore = $data['lowestScore'];
        $this->totalScore = $data['totalScore'];
        $this->index = $data['index'];
    }

    public function broadcastOn()
    {
        return ['edsfuncade'];
    }

    /**
     * Get the broadcast event name.
     *
     * @return string
     */
    public function broadcastAs()
    {
        return 'round.score';
    }
}
