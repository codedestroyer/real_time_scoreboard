<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class GameStarted
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $gameId;
    public $location;

    public function __construct($data)
    {
        $this->gameId = $data['gameId'];
        $this->location = $data['location'];
    }

    public function broadcastOn()
    {
        return ['edsfuncade'];
    }

    /**
     * Get the broadcast event name.
     *
     * @return string
     */
    public function broadcastAs()
    {
        return 'new.game';
    }
}
