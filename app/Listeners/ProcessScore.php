<?php

namespace App\Listeners;

use App\Events\ScoreAdded;
use App\Repositories\PlayerRepo;
use App\Repositories\ScoreRepo;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;
class ProcessScore
{
    /**
     * Create the event listener.
     *
     * @return void
     *
     */

    private $scoreRep;
    private $player;
    public function __construct(ScoreRepo $repo, PlayerRepo $player)
    {
        $this->scoreRep = $repo;
        $this->player = $player;
    }

    /**
     * Handle the event.
     *
     * @param  ScoreAdded  $event
     * @return boolean
     */
    public function handle(ScoreAdded $event)
    {
        $playerId = $this->player->createOrReturnPlayer($event->firstName, $event->lastName, $event->uniqueKey)->id;
        $this->scoreRep->insertScore($playerId, $event->gameId, $event->score);
        return true;
    }
}
