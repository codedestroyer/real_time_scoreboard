<?php

namespace App\Listeners;

use App\Repositories\PlayerRepo;
use App\Events\ScoreUpdated;
use App\Repositories\ScoreRepo;
use Log;

class UpdateScore
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    private $scoreRep;
    private $player;
    public function __construct(ScoreRepo $repo, PlayerRepo $player)
    {
        $this->scoreRep = $repo;
        $this->player = $player;
    }

    /**
     * Handle the event.
     *
     * @param  ScoreAdded  $event
     * @return bool
     */
    public function handle(ScoreUpdated $event)
    {
        return true;
    }
}
