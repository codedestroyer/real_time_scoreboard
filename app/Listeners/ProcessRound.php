<?php

namespace App\Listeners;

use App\Repositories\PlayerRepo;
use App\Events\RoundAdded;
use App\Repositories\ScoreRepo;
use Log;

class ProcessRound
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    private $scoreRep;
    private $player;
    public function __construct(ScoreRepo $repo, PlayerRepo $player)
    {
        $this->scoreRep = $repo;
        $this->player = $player;
    }

    /**
     * Handle the event.
     *
     * @param  RoundAdded  $event
     * @return bool
     */
    public function handle(RoundAdded $event)
    {
       

        return true;
    }
}
