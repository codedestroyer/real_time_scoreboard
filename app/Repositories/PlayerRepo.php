<?php
/**
 * Created by PhpStorm.
 * User: pcunningham
 * Date: 5/11/17
 * Time: 12:25 PM
 */

namespace App\Repositories;


use App\Models\Player;

class PlayerRepo
{
    private $model;

    public function __construct(Player $player)
    {
        $this->model = $player;
    }



    public function createOrReturnPlayer($firstName, $lastName, $key){
        return $this->model->updateOrCreate(
            ['firstName' => $firstName,
             'lastName'  => $lastName
            ],['firstName' => $firstName,
                'lastName'  => $lastName,
                'uniqueKey' => $key
            ]);
    }

    public function getByKey($key){
        return $this->model->where("uniqueKey",$key)->firstOrFail();
    }

}