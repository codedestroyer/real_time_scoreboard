<?php
/**
 * Created by PhpStorm.
 * User: pcunningham
 * Date: 6/13/17
 * Time: 3:37 PM
 */

namespace App\Repositories;


use App\Models\GameRules;

class GamesRulesRepo
{
    protected $gameRules;

    public function __construct(GameRules $gameRules)
    {
        $this->gameRules = $gameRules;
    }

    public function getRules($id)
    {
        return $this->gameRules->find($id);
    }

}