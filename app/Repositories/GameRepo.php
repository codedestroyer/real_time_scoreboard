<?php
/**
 * Created by PhpStorm.
 * User: pcunningham
 * Date: 5/11/17
 * Time: 12:25 PM.
 */

namespace App\Repositories;

use App\Models\Game;

class GameRepo
{
    private $model;

    public function __construct(Game $game)
    {
        $this->model = $game;
    }

    public function createGame($data)
    {
        return $this->model->create($data);
    }
    
}
