<?php

namespace App\Repositories;
use App\Models\Score;
use Illuminate\Support\Facades\DB;
class ScoreRepo
{
    private $model;

    public function __construct(Score $score)
    {
        $this->model = $score;
    }


    public function insertScore($playerId, $gameId, $score){
        return $this->model->create(
            ['playerId'  => $playerId,
              'gameId'   => $gameId,
              'score'    => $score]
        );
    }

    public function getTotals($playerId, $gameId){
        $currentGame = 1;
        $total = 0;
        $lowest = 900;
        $scores = $this->model->select("score")
                                ->where("playerId", $playerId)
                                ->where("gameId",$gameId)
                                ->orderBy("score",'desc')->get();
        foreach($scores->pluck("score") as $score){
            if($currentGame <=5){
                $total+=$score;
                if($lowest > $score){
                    $lowest = $score;
                }
            }
            $currentGame++;
        }
        return ["total"=> $total, "lowest" => $lowest];
    }

    public function getHighest($playerId, $gameId){
        return $this->model->select("score")
            ->where("playerId", $playerId)
            ->where("gameId",$gameId)
            ->orderBy("score",'desc')->first();
    }

    public function getScores($id,$gameId){
      return $this->model->where("gameId",$gameId)->where('playerId',$id)->get();
    }
}
