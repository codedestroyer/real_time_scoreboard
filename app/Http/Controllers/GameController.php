<?php

namespace App\Http\Controllers;

use App\Events\RoundAdded;
use App\Events\ScoreAdded;
use App\Events\ScoreUpdated;
use App\Repositories\GameRepo;
use App\Repositories\GamesRulesRepo;
use App\Repositories\PlayerRepo;
use App\Repositories\ScoreRepo;
use Illuminate\Http\Request;

use Illuminate\View\Concerns\ManagesEvents;

class GameController extends Controller
{
    use ManagesEvents;
    private $gameRepo;
    private $rulesRepo;
    private $scoreRepo;
    private $playerRepo;

    public function __construct(GameRepo $gameRepo, GamesRulesRepo $gamesRulesRepo, ScoreRepo $scoreRepo, PlayerRepo $playerRepo)
    {
        $this->gameRepo = $gameRepo;
        $this->rulesRepo = $gamesRulesRepo;
        $this->scoreRepo = $scoreRepo;
        $this->playerRepo = $playerRepo;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [
                'gameRulesId'     => $request->input('gameType'),
                 'locationId'     => $request->input('location'),
        ];
        $game = $this->gameRepo->createGame($data);
        $rules = $this->rulesRepo->getRules($request->input('gameType'));
        return response()->json(array_merge($game->toArray(),json_decode($rules->config,true)));
    }

    /**
 * Store a newly created resource in storage.
 *
 * @param  \Illuminate\Http\Request  $request
 * @return \Illuminate\Http\Response
 */
    public function storeScore(Request $request)
    {
        $data = ['gameId'       => $request->input('gameId'),
            'location'  => $request->input('location'),
            'firstName' => $request->input('firstName'),
            'lastName'  => $request->input('lastName'),
            'score'     => $request->input('score'),
            'key'     => $request->input('key'),
        ];
        event(new ScoreAdded($data));

        return response()->json(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeRound(Request $request)
    {
        $data = [
            'key'      => $request->input("key"),
            'gameId'   => $request->input('gameId'),
            'location'     => $request->input('location'),
            'score'        => $request->input('score'),
        ];
        event(new RoundAdded($data));

        return response()->json(true);
    }

    public function updateScore(Request $request)
    {
        $score = $request->input('score');
        $gameId = $request->input('gameId');
        $player = $this->playerRepo->getByKey($request->input("key"));
        $this->scoreRepo->insertScore($player->id,$gameId,$score);
        $score = $this->scoreRepo->getHighest($player->id, $gameId);
        $data = [
            'gameId'     => $request->input('gameId'),
            'location'   => $request->input('location'),
            'score'      => $score->score,
            'index'      => $request->input('index')
        ];
        
        event(new ScoreUpdated($data));
        return response()->json(true);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addScoreTotal(Request $request)
    {
        $score = $request->input('score');
        $gameId = $request->input('gameId');
        $player = $this->playerRepo->getByKey($request->input("key"));
        $this->scoreRepo->insertScore($player->id,$gameId,$score);
        $scoreArray = $this->scoreRepo->getTotals($player->id, $gameId);

             $data = [
                 'key'      => $request->input("key"),
                 'gameId'   => $gameId,
                 'location'     => $request->input('location'),
                 'lowestScore'  => $scoreArray['lowest'],
                 'totalScore' => $scoreArray['total'],
                 'index'      => $request->input('index')
             ];
        event(new RoundAdded($data));

        return response()->json(true);
    }

    public function getScores(Request $request){
        $player = $this->playerRepo->getByKey($request->input("key"));
        return $this->scoreRepo->getScores($player->id,$request->input("gameId"))->pluck("score");
    }



}
