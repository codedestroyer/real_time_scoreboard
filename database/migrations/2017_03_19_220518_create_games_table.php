<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("gameRulesId");
            $table->integer("locationId");
            $table->integer("totalPlayers");
            $table->integer("userId");
            $table->integer("winner");
            $table->json("results");
            $table->dateTime("timeStarted");
            $table->dateTime("timeFinished");
            $table->boolean("gameWasModified");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}
