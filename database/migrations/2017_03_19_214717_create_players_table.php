<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('players', function (Blueprint $table) {
            $table->increments('id');
            $table->string("firstName");
            $table->string("lastName");
            $table->string("passCode")->nullable();
            $table->float("averageScore")->nullable();
            $table->integer("topScore")->nullable();
            $table->integer("totalGamesPlayed")->nullable();
            $table->float("averageGamesPlayedPerVisit")->nullable();
            $table->string("photoUrl")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('players');
    }
}
