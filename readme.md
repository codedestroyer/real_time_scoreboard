#Getting Started - Vet Portal
##Prerequisites
*	Virtual Box Installed - https://www.virtualbox.org/wiki/Downloads
* Vagrant Installed - https://www.vagrantup.com



##Getting Started
3. then `composer install`
4.	run `cat .env.example > .env` this moves some base settings into your local .`env` file.
5. edit the env file with `vi .env`
6.	lets generate some encryption keys now.  run `php artisan key:generate`
7. run `npm install` to get the front end started.
8. followed up with `bower install` and finally `gulp buid`

7. Lets build the database.  run `php artisan migrate`.
8.	Run import of specific 

Line 16 of app.js in resources needs this edited to whatever url being used
  return socketFactory({ioSocket: io.connect('local.skeeball.com:6001/',
  
  redis needs to be running.  node runs a pub/sub from redis then uses IO to talk to rooms.
  i use something like pm2 to make sure server.js is running 
  
  
 js views are in public due to lazy on gulp to properly move from resources.
 
 There is an entire 2nd half to the app that is half done working but not fleshed out since some major performance updates based on eds/steves needs on ipad
(i didnt expect them to have 20 people with like 50 games entered in)
the other half of the app is seeing a display board with socket.  the current setup pushes way to much information at once.  planned on looking into it.

all the angular js follows a simple component structure, and use router UI to handle requests.  
GameState holds the data that goes across pages. 
