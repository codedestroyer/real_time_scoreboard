
angular
.module('app', [
  'btford.socket-io',
  'bc.AngularKeypad',
  'ui.router',
  'ngSanitize',
  'ngAnimate',
  'timer',
  'toaster',
  'ngDialog',
  'dndLists'
]);
angular.module('app').
factory('socket', function (socketFactory) {
  return socketFactory({ioSocket: io.connect('local.skeeball.com:6001/',{
    reconnection: true,
    reconnectionDelay: 1000,
    reconnectionDelayMax : 5000,
    reconnectionAttempts: Infinity
  })});
}).
value('version', '0.9');

/**
 * angular-drag-and-drop-lists v2.1.0
 *
 * Copyright (c) 2014 Marcel Juenemann marcel@juenemann.cc
 * Copyright (c) 2014-2017 Google Inc.
 * https://github.com/marceljuenemann/angular-drag-and-drop-lists
 *
 * License: MIT
 */
!function(e){function n(e,n){return"all"==n?e:e.filter(function(e){return-1!=n.toLowerCase().indexOf(e)})}var a="application/x-dnd",r="application/json",t="Text",d=["move","copy","link"]
    e.directive("dndDraggable",["$parse","$timeout",function(e,i){return function(l,f,c){f.attr("draggable","true"),c.dndDisableIf&&l.$watch(c.dndDisableIf,function(e){f.attr("draggable",!e)}),f.on("dragstart",function(s){if(s=s.originalEvent||s,"false"==f.attr("draggable"))return!0
        o.isDragging=!0,o.itemType=c.dndType&&l.$eval(c.dndType).toLowerCase(),o.dropEffect="none",o.effectAllowed=c.dndEffectAllowed||d[0],s.dataTransfer.effectAllowed=o.effectAllowed
        var g=l.$eval(c.dndDraggable),u=a+(o.itemType?"-"+o.itemType:"")
        try{s.dataTransfer.setData(u,angular.toJson(g))}catch(p){var v=angular.toJson({item:g,type:o.itemType})
            try{s.dataTransfer.setData(r,v)}catch(p){var D=n(d,o.effectAllowed)
                s.dataTransfer.effectAllowed=D[0],s.dataTransfer.setData(t,v)}}if(f.addClass("dndDragging"),i(function(){f.addClass("dndDraggingSource")},0),s._dndHandle&&s.dataTransfer.setDragImage&&s.dataTransfer.setDragImage(f[0],0,0),e(c.dndDragstart)(l,{event:s}),c.dndCallback){var y=e(c.dndCallback)
            o.callback=function(e){return y(l,e||{})}}s.stopPropagation()}),f.on("dragend",function(n){n=n.originalEvent||n,l.$apply(function(){var a=o.dropEffect,r={copy:"dndCopied",link:"dndLinked",move:"dndMoved",none:"dndCanceled"}
        e(c[r[a]])(l,{event:n}),e(c.dndDragend)(l,{event:n,dropEffect:a})}),o.isDragging=!1,o.callback=void 0,f.removeClass("dndDragging"),f.removeClass("dndDraggingSource"),n.stopPropagation(),i(function(){f.removeClass("dndDraggingSource")},0)}),f.on("click",function(n){c.dndSelected&&(n=n.originalEvent||n,l.$apply(function(){e(c.dndSelected)(l,{event:n})}),n.stopPropagation())}),f.on("selectstart",function(){this.dragDrop&&this.dragDrop()})}}]),e.directive("dndList",["$parse",function(e){return function(i,l,f){function c(e){if(!e)return t
        for(var n=0;n<e.length;n++)if(e[n]==t||e[n]==r||e[n].substr(0,a.length)==a)return e[n]
        return null}function s(e){return o.isDragging?o.itemType||void 0:e==t||e==r?null:e&&e.substr(a.length+1)||void 0}function g(e){return E.disabled?!1:E.externalSources||o.isDragging?E.allowedTypes&&null!==e?e&&-1!=E.allowedTypes.indexOf(e):!0:!1}function u(e,a){var r=d
        return a||(r=n(r,e.dataTransfer.effectAllowed)),o.isDragging&&(r=n(r,o.effectAllowed)),f.dndEffectAllowed&&(r=n(r,f.dndEffectAllowed)),r.length?e.ctrlKey&&-1!=r.indexOf("copy")?"copy":e.altKey&&-1!=r.indexOf("link")?"link":r[0]:"none"}function p(){return T.remove(),l.removeClass("dndDragover"),!0}function v(n,a,r,t,d,l){return e(n)(i,{callback:o.callback,dropEffect:r,event:a,external:!o.isDragging,index:void 0!==d?d:D(),item:l||void 0,type:t})}function D(){return Array.prototype.indexOf.call(m.children,h)}function y(){var e
        return angular.forEach(l.children(),function(n){var a=angular.element(n)
            a.hasClass("dndPlaceholder")&&(e=a)}),e||angular.element("<li class='dndPlaceholder'></li>")}var T=y()
        T.remove()
        var h=T[0],m=l[0],E={}
        l.on("dragenter",function(e){e=e.originalEvent||e
            var n=f.dndAllowedTypes&&i.$eval(f.dndAllowedTypes)
            E={allowedTypes:angular.isArray(n)&&n.join("|").toLowerCase().split("|"),disabled:f.dndDisableIf&&i.$eval(f.dndDisableIf),externalSources:f.dndExternalSources&&i.$eval(f.dndExternalSources),horizontal:f.dndHorizontalList&&i.$eval(f.dndHorizontalList)}
            var a=c(e.dataTransfer.types)
            return a&&g(s(a))?void e.preventDefault():!0}),l.on("dragover",function(e){e=e.originalEvent||e
            var n=c(e.dataTransfer.types),a=s(n)
            if(!n||!g(a))return!0
            if(h.parentNode!=m&&l.append(T),e.target!=m){for(var r=e.target;r.parentNode!=m&&r.parentNode;)r=r.parentNode
                if(r.parentNode==m&&r!=h){var d=r.getBoundingClientRect()
                    if(E.horizontal)var o=e.clientX<d.left+d.width/2
                    else var o=e.clientY<d.top+d.height/2
                    m.insertBefore(h,o?r:r.nextSibling)}}var i=n==t,D=u(e,i)
            return"none"==D?p():f.dndDragover&&!v(f.dndDragover,e,D,a)?p():(e.preventDefault(),i||(e.dataTransfer.dropEffect=D),l.addClass("dndDragover"),e.stopPropagation(),!1)}),l.on("drop",function(e){e=e.originalEvent||e
            var n=c(e.dataTransfer.types),a=s(n)
            if(!n||!g(a))return!0
            e.preventDefault()
            try{var d=JSON.parse(e.dataTransfer.getData(n))}catch(l){return p()}if((n==t||n==r)&&(a=d.type||void 0,d=d.item,!g(a)))return p()
            var y=n==t,T=u(e,y)
            if("none"==T)return p()
            var h=D()
            return f.dndDrop&&(d=v(f.dndDrop,e,T,a,h,d),!d)?p():(o.dropEffect=T,y||(e.dataTransfer.dropEffect=T),d!==!0&&i.$apply(function(){i.$eval(f.dndList).splice(h,0,d)}),v(f.dndInserted,e,T,a,h,d),p(),e.stopPropagation(),!1)}),l.on("dragleave",function(e){e=e.originalEvent||e
            var n=document.elementFromPoint(e.clientX,e.clientY)
            m.contains(n)&&!e._dndPhShown?e._dndPhShown=!0:p()})}}]),e.directive("dndNodrag",function(){return function(e,n,a){n.attr("draggable","true"),n.on("dragstart",function(e){e=e.originalEvent||e,e._dndHandle||(e.dataTransfer.types&&e.dataTransfer.types.length||e.preventDefault(),e.stopPropagation())}),n.on("dragend",function(e){e=e.originalEvent||e,e._dndHandle||e.stopPropagation()})}}),e.directive("dndHandle",function(){return function(e,n,a){n.attr("draggable","true"),n.on("dragstart dragend",function(e){e=e.originalEvent||e,e._dndHandle=!0})}})
    var o={}}(angular.module("dndLists",[]));
var DragDropTouch;
(function (DragDropTouch_1) {
    'use strict';
    /**
     * Object used to hold the data that is being dragged during drag and drop operations.
     *
     * It may hold one or more data items of different types. For more information about
     * drag and drop operations and data transfer objects, see
     * <a href="https://developer.mozilla.org/en-US/docs/Web/API/DataTransfer">HTML Drag and Drop API</a>.
     *
     * This object is created automatically by the @see:DragDropTouch singleton and is
     * accessible through the @see:dataTransfer property of all drag events.
     */
    var DataTransfer = (function () {
        function DataTransfer() {
            this._dropEffect = 'move';
            this._effectAllowed = 'all';
            this._data = {};
        }
        Object.defineProperty(DataTransfer.prototype, "dropEffect", {
            /**
             * Gets or sets the type of drag-and-drop operation currently selected.
             * The value must be 'none',  'copy',  'link', or 'move'.
             */
            get: function () {
                return this._dropEffect;
            },
            set: function (value) {
                this._dropEffect = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DataTransfer.prototype, "effectAllowed", {
            /**
             * Gets or sets the types of operations that are possible.
             * Must be one of 'none', 'copy', 'copyLink', 'copyMove', 'link',
             * 'linkMove', 'move', 'all' or 'uninitialized'.
             */
            get: function () {
                return this._effectAllowed;
            },
            set: function (value) {
                this._effectAllowed = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DataTransfer.prototype, "types", {
            /**
             * Gets an array of strings giving the formats that were set in the @see:dragstart event.
             */
            get: function () {
                return Object.keys(this._data);
            },
            enumerable: true,
            configurable: true
        });
        /**
         * Removes the data associated with a given type.
         *
         * The type argument is optional. If the type is empty or not specified, the data
         * associated with all types is removed. If data for the specified type does not exist,
         * or the data transfer contains no data, this method will have no effect.
         *
         * @param type Type of data to remove.
         */
        DataTransfer.prototype.clearData = function (type) {
            if (type != null) {
                delete this._data[type];
            }
            else {
                this._data = null;
            }
        };
        /**
         * Retrieves the data for a given type, or an empty string if data for that type does
         * not exist or the data transfer contains no data.
         *
         * @param type Type of data to retrieve.
         */
        DataTransfer.prototype.getData = function (type) {
            return this._data[type] || '';
        };
        /**
         * Set the data for a given type.
         *
         * For a list of recommended drag types, please see
         * https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/Recommended_Drag_Types.
         *
         * @param type Type of data to add.
         * @param value Data to add.
         */
        DataTransfer.prototype.setData = function (type, value) {
            this._data[type] = value;
        };
        /**
         * Set the image to be used for dragging if a custom one is desired.
         *
         * @param img An image element to use as the drag feedback image.
         * @param offsetX The horizontal offset within the image.
         * @param offsetY The vertical offset within the image.
         */
        DataTransfer.prototype.setDragImage = function (img, offsetX, offsetY) {
            var ddt = DragDropTouch._instance;
            ddt._imgCustom = img;
            ddt._imgOffset = { x: offsetX, y: offsetY };
        };
        return DataTransfer;
    }());
    DragDropTouch_1.DataTransfer = DataTransfer;
    /**
     * Defines a class that adds support for touch-based HTML5 drag/drop operations.
     *
     * The @see:DragDropTouch class listens to touch events and raises the
     * appropriate HTML5 drag/drop events as if the events had been caused
     * by mouse actions.
     *
     * The purpose of this class is to enable using existing, standard HTML5
     * drag/drop code on mobile devices running IOS or Android.
     *
     * To use, include the DragDropTouch.js file on the page. The class will
     * automatically start monitoring touch events and will raise the HTML5
     * drag drop events (dragstart, dragenter, dragleave, drop, dragend) which
     * should be handled by the application.
     *
     * For details and examples on HTML drag and drop, see
     * https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/Drag_operations.
     */
    var DragDropTouch = (function () {
        /**
         * Initializes the single instance of the @see:DragDropTouch class.
         */
        function DragDropTouch() {
            this._lastClick = 0;
            // enforce singleton pattern
            if (DragDropTouch._instance) {
                throw 'DragDropTouch instance already created.';
            }
            // detect passive event support
            // https://github.com/Modernizr/Modernizr/issues/1894
            var supportsPassive = false;
            document.addEventListener('test', null, {
                get passive() {
                    supportsPassive = true;
                    return true;
                }
            });
            // listen to touch events
            if ('ontouchstart' in document) {
                var d = document, ts = this._touchstart.bind(this), tm = this._touchmove.bind(this), te = this._touchend.bind(this), opt = supportsPassive ? { passive: false, capture: false } : false;
                d.addEventListener('touchstart', ts, opt);
                d.addEventListener('touchmove', tm, opt);
                d.addEventListener('touchend', te);
                d.addEventListener('touchcancel', te);
            }
        }
        /**
         * Gets a reference to the @see:DragDropTouch singleton.
         */
        DragDropTouch.getInstance = function () {
            return DragDropTouch._instance;
        };
        // ** event handlers
        DragDropTouch.prototype._touchstart = function (e) {
            var _this = this;
            if (this._shouldHandle(e)) {
                // raise double-click and prevent zooming
                if (Date.now() - this._lastClick < DragDropTouch._DBLCLICK) {
                    if (this._dispatchEvent(e, 'dblclick', e.target)) {
                        e.preventDefault();
                        this._reset();
                        return;
                    }
                }
                // clear all variables
                this._reset();
                // get nearest draggable element
                var src = this._closestDraggable(e.target);
                if (src) {
                    // give caller a chance to handle the hover/move events
                    if (!this._dispatchEvent(e, 'mousemove', e.target) &&
                        !this._dispatchEvent(e, 'mousedown', e.target)) {
                        // get ready to start dragging
                        this._dragSource = src;
                        this._ptDown = this._getPoint(e);
                        this._lastTouch = e;
                        e.preventDefault();
                        // show context menu if the user hasn't started dragging after a while
                        setTimeout(function () {
                            if (_this._dragSource == src && _this._img == null) {
                                if (_this._dispatchEvent(e, 'contextmenu', src)) {
                                    _this._reset();
                                }
                            }
                        }, DragDropTouch._CTXMENU);
                    }
                }
            }
        };
        DragDropTouch.prototype._touchmove = function (e) {
            if (this._shouldHandle(e)) {
                // see if target wants to handle move
                var target = this._getTarget(e);
                if (this._dispatchEvent(e, 'mousemove', target)) {
                    this._lastTouch = e;
                    e.preventDefault();
                    return;
                }
                // start dragging
                if (this._dragSource && !this._img) {
                    var delta = this._getDelta(e);
                    if (delta > DragDropTouch._THRESHOLD) {
                        this._dispatchEvent(e, 'dragstart', this._dragSource);
                        this._createImage(e);
                        this._dispatchEvent(e, 'dragenter', target);
                    }
                }
                // continue dragging
                if (this._img) {
                    this._lastTouch = e;
                    e.preventDefault(); // prevent scrolling
                    if (target != this._lastTarget) {
                        this._dispatchEvent(this._lastTouch, 'dragleave', this._lastTarget);
                        this._dispatchEvent(e, 'dragenter', target);
                        this._lastTarget = target;
                    }
                    this._moveImage(e);
                    this._dispatchEvent(e, 'dragover', target);
                }
            }
        };
        DragDropTouch.prototype._touchend = function (e) {
            if (this._shouldHandle(e)) {
                // see if target wants to handle up
                if (this._dispatchEvent(this._lastTouch, 'mouseup', e.target)) {
                    e.preventDefault();
                    return;
                }
                // user clicked the element but didn't drag, so clear the source and simulate a click
                if (!this._img) {
                    this._dragSource = null;
                    this._dispatchEvent(this._lastTouch, 'click', e.target);
                    this._lastClick = Date.now();
                }
                // finish dragging
                this._destroyImage();
                if (this._dragSource) {
                    if (e.type.indexOf('cancel') < 0) {
                        this._dispatchEvent(this._lastTouch, 'drop', this._lastTarget);
                    }
                    this._dispatchEvent(this._lastTouch, 'dragend', this._dragSource);
                    this._reset();
                }
            }
        };
        // ** utilities
        // ignore events that have been handled or that involve more than one touch
        DragDropTouch.prototype._shouldHandle = function (e) {
            return e &&
                !e.defaultPrevented &&
                e.touches && e.touches.length < 2;
        };
        // clear all members
        DragDropTouch.prototype._reset = function () {
            this._destroyImage();
            this._dragSource = null;
            this._lastTouch = null;
            this._lastTarget = null;
            this._ptDown = null;
            this._dataTransfer = new DataTransfer();
        };
        // get point for a touch event
        DragDropTouch.prototype._getPoint = function (e, page) {
            if (e && e.touches) {
                e = e.touches[0];
            }
            return { x: page ? e.pageX : e.clientX, y: page ? e.pageY : e.clientY };
        };
        // get distance between the current touch event and the first one
        DragDropTouch.prototype._getDelta = function (e) {
            var p = this._getPoint(e);
            return Math.abs(p.x - this._ptDown.x) + Math.abs(p.y - this._ptDown.y);
        };
        // get the element at a given touch event
        DragDropTouch.prototype._getTarget = function (e) {
            var pt = this._getPoint(e), el = document.elementFromPoint(pt.x, pt.y);
            while (el && getComputedStyle(el).pointerEvents == 'none') {
                el = el.parentElement;
            }
            return el;
        };
        // create drag image from source element
        DragDropTouch.prototype._createImage = function (e) {
            // just in case...
            if (this._img) {
                this._destroyImage();
            }
            // create drag image from custom element or drag source
            var src = this._imgCustom || this._dragSource;
            this._img = src.cloneNode(true);
            this._copyStyle(src, this._img);
            this._img.style.top = this._img.style.left = '-9999px';
            // if creating from drag source, apply offset and opacity
            if (!this._imgCustom) {
                var rc = src.getBoundingClientRect(), pt = this._getPoint(e);
                this._imgOffset = { x: pt.x - rc.left, y: pt.y - rc.top };
                this._img.style.opacity = DragDropTouch._OPACITY.toString();
            }
            // add image to document
            this._moveImage(e);
            document.body.appendChild(this._img);
        };
        // dispose of drag image element
        DragDropTouch.prototype._destroyImage = function () {
            if (this._img && this._img.parentElement) {
                this._img.parentElement.removeChild(this._img);
            }
            this._img = null;
            this._imgCustom = null;
        };
        // move the drag image element
        DragDropTouch.prototype._moveImage = function (e) {
            var _this = this;
            if (this._img) {
                requestAnimationFrame(function () {
                    var pt = _this._getPoint(e, true), s = _this._img.style;
                    s.position = 'absolute';
                    s.pointerEvents = 'none';
                    s.zIndex = '999999';
                    s.left = Math.round(pt.x - _this._imgOffset.x) + 'px';
                    s.top = Math.round(pt.y - _this._imgOffset.y) + 'px';
                });
            }
        };
        // copy properties from an object to another
        DragDropTouch.prototype._copyProps = function (dst, src, props) {
            for (var i = 0; i < props.length; i++) {
                var p = props[i];
                dst[p] = src[p];
            }
        };
        DragDropTouch.prototype._copyStyle = function (src, dst) {
            // remove potentially troublesome attributes
            DragDropTouch._rmvAtts.forEach(function (att) {
                dst.removeAttribute(att);
            });
            // copy canvas content
            if (src instanceof HTMLCanvasElement) {
                var cSrc = src, cDst = dst;
                cDst.width = cSrc.width;
                cDst.height = cSrc.height;
                cDst.getContext('2d').drawImage(cSrc, 0, 0);
            }
            // copy style (without transitions)
            var cs = getComputedStyle(src);
            for (var i = 0; i < cs.length; i++) {
                var key = cs[i];
                if (key.indexOf('transition') < 0) {
                    dst.style[key] = cs[key];
                }
            }
            dst.style.pointerEvents = 'none';
            // and repeat for all children
            for (var i = 0; i < src.children.length; i++) {
                this._copyStyle(src.children[i], dst.children[i]);
            }
        };
        DragDropTouch.prototype._dispatchEvent = function (e, type, target) {
            if (e && target) {
                var evt = document.createEvent('Event'), t = e.touches ? e.touches[0] : e;
                evt.initEvent(type, true, true);
                evt.button = 0;
                evt.which = evt.buttons = 1;
                this._copyProps(evt, e, DragDropTouch._kbdProps);
                this._copyProps(evt, t, DragDropTouch._ptProps);
                evt.dataTransfer = this._dataTransfer;
                target.dispatchEvent(evt);
                return evt.defaultPrevented;
            }
            return false;
        };
        // gets an element's closest draggable ancestor
        DragDropTouch.prototype._closestDraggable = function (e) {
            for (; e; e = e.parentElement) {
                if (e.hasAttribute('draggable') && e.draggable) {
                    return e;
                }
            }
            return null;
        };
        return DragDropTouch;
    }());
    /*private*/ DragDropTouch._instance = new DragDropTouch(); // singleton
    // constants
    DragDropTouch._THRESHOLD = 5; // pixels to move before drag starts
    DragDropTouch._OPACITY = 0.5; // drag image opacity
    DragDropTouch._DBLCLICK = 500; // max ms between clicks in a double click
    DragDropTouch._CTXMENU = 900; // ms to hold before raising 'contextmenu' event
    // copy styles/attributes from drag source to drag image element
    DragDropTouch._rmvAtts = 'id,class,style,draggable'.split(',');
    // synthesize and dispatch an event
    // returns true if the event has been handled (e.preventDefault == true)
    DragDropTouch._kbdProps = 'altKey,ctrlKey,metaKey,shiftKey'.split(',');
    DragDropTouch._ptProps = 'pageX,pageY,clientX,clientY,screenX,screenY'.split(',');
    DragDropTouch_1.DragDropTouch = DragDropTouch;
})(DragDropTouch || (DragDropTouch = {}));
/**
 * Created by codedestroyer on 5/8/17.
 */
angular
    .module('app')
    .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'views/main.html'
            }).state('entry', {
                url: '/game',
                templateUrl: 'views/game.html'
            }).state('bestOf', {
            url: '/bestof',
            templateUrl: 'views/bestOf.html'
        }).state('dragboard', {
            url: '/drag',
            templateUrl: 'views/dragboard.html'
        }).state('elimination', {
                url: '/grid',
                templateUrl: 'views/grid.html'
            }).state('scoreboard', {
            url: '/scoreboard',
            templateUrl: 'views/scoreboard.html'

        }).state('gridView', {
        url: '/gridView',
        templateUrl: 'views/gridView.html'

    }).state('gameOver', {
            url: '/gameOver',
            templateUrl: 'views/gameOver.html'

        });
        angular.module('app').run(['$http', '$urlRouter', function ($http, runtimeStates, $urlRouter) {

        }]);
    }]);

(function () {
    angular.module('app').factory('gameApi', gameApi);
    gameApi.$inject = ['$http'];
    function gameApi($http) {
        return {
            submitScore: function (gameId,location,first, last, score, key) {
                return $http.post('/api/game/score',
                     {
                            gameId: gameId,
                            location:location,
                            firstName: first,
                            lastName: last,
                            score: score,
                            key: key
                            }
                );
            },
            submitRound: function (gameId,location,first, last, score, key) {
                return $http.post('/api/game/round',
                    {
                        gameId: gameId,
                        location:location,
                        firstName: first,
                        lastName: last,
                        score: score,
                        key: key}
                );
            },
                submitTotal: function (gameId,location, score, key, index) {
                    return $http.post('/api/game/addTotal',
                        {
                            gameId: gameId,
                            location:location,
                            score: score,
                            index: index,
                            key: key}
                    );
                },
            updateScore: function (gameId,location, score, key, index) {
                return $http.patch('/api/game/score',
                     {

                         gameId: gameId,
                         location:location,
                         score: score,
                         index: index,
                         key: key}
                );
            },
            createGame: function (location, gameType) {
                return $http.post('/api/game/',
                     {
                            location:location,
                            gameType: gameType}
                );
            },
            getGames: function (key,gameId){
                return $http.get("api/game/list",{params: { key: key, gameId:gameId}});
            }
        };
    }
})();

(function () {
    angular.module('app').factory('locationApi', locationApi);
    locationApi.$inject = ['$http'];
    function locationApi($http) {
        return {
            getLocations: function () {
                return $http.get('/api/location');
            }
        };
    }
})();

(function() {
    angular.module('app').component('bestof', {
        templateUrl: 'views/components/bestof.html',
        controller: ['gameController', 'ngDialog', '$state', 'socket', 'gameApi', '$scope', '$timeout', ctrl],
        controllerAs: 'vm'
    });

    function ctrl(gameController, ngDialog, $state, socket, gameApi, $scope, $timeout) {
        var vm = this;
        vm.currentRound = 0;
        vm.game = gameController;
        vm.roundScore = "";
        vm.currentRoundRules = vm.game.rounds[vm.currentRound];
        vm.newPlayer ={firstName: "",lastName:"",score:""};
        vm.$onInit = function() {
            vm.locationId = gameController.locationId;
            vm.gameId = gameController.gameId;
            if (vm.locationId == 0 || vm.gameId == 0) {
                $state.go("home");
            } else {
                socket.emit('change room', vm.locationId);
            }
        };
        
        $scope.$on('$destroy', function (event) {
            socket.removeAllListeners();
            console.log("destroyed");
        });

        vm.addScorePopUp = function() {
            ngDialog.open({
                template: '/views/popups/newScore.html',
                scope: $scope,
                showClose: false
            });
        };

        vm.addScore = function() {
            if(vm.newPlayer.firstName !== "" && vm.newPlayer.lastName!=="") {
                gameApi.submitScore(vm.gameId, vm.locationId, vm.newPlayer.firstName, vm.newPlayer.lastName, vm.newPlayer.score, vm.game.randomString(32))
                    .then(function (response) {
                        ngDialog.close();
                        vm.newPlayer.firstName = "";
                        vm.newPlayer.lastName = "";
                        vm.newPlayer.score = "";
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }
        };

        /** Add Game to PLayer **/
        vm.addRoundPopup = function(index) {
            gameController.players[index].show = false;
            vm.roundPlayer = gameController.players[index];
            vm.roundPlayer.index = index;
            roundDialog = ngDialog.open({
                template: '/views/popups/roundScore.html',
                scope: $scope,
                showClose: false
            });
            roundDialog.closePromise.then(function (data) {
                delete vm.roundPlayer;
                vm.roundScore = "";
                $timeout(function() {
                    gameController.players[index].show = true;
                }, 500);
            });
        };

        vm.addRound = function (){
            gameApi.submitTotal(vm.gameId, vm.locationId, vm.roundScore, vm.roundPlayer.key, vm.roundPlayer.index)
                .then(function(response) {
                    ngDialog.close();
                    vm.roundScore = "";
                    vm.roundPlayer = {};
                })
                .catch(function(error) {
                    console.log(error);
                });
        };
        
        
        vm.removePlayer = function (index){
            if(gameController.players[index].playerClear){
                deletePlayer = confirm("Are you sure you want to reactivate user?");
                if(deletePlayer){
                    gameController.players[index].playerClear = false;
                }
            } else {
                deletePlayer = confirm("Are you sure you want to Inactive user?");
                if(deletePlayer){
                    gameController.players[index].playerClear = true;
                }
            }


        };


        //TODO UPDATE
        vm.viewGames = function (index){
            vm.roundPlayer = gameController.players[index];
            gameApi.getGames(vm.roundPlayer.key,vm.gameId).then(function(response){
                vm.tempScore = response.data;
                dialog = ngDialog.open({
                    template: '/views/popups/viewScores.html',
                    scope: $scope,
                    showClose: false
                });
                dialog.closePromise.then(function (data) {
                    delete vm.roundPlayer;
                    delete vm.tempScore;
                });
            });
        };



        /** Socket Events **/
        socket.on("new.score", function (data) {
                gameController.players.push({
                    name: data.firstName + " " + data.lastName,
                    firstName: data.firstName,
                    lastName: data.lastName,
                    score: parseInt(data.score),
                    totalScore: parseInt(data.score),
                    lowestScore: parseInt(data.score),
                    key: data.uniqueKey,
                    show:true,
                    playerClear:false,
                    gamesPlayed: 1
                });
        });

        //during elimination
        socket.on('round.score',function(data){
            console.log("new score");
            gameController.players[data.index].totalScore = data.totalScore;
            gameController.players[data.index].lowestScore = data.lowestScore;
            gameController.players[data.index].gamesPlayed++;
            gameController.players[data.index].show =true;
           // socket.emit("updatePlayers", {room: game.locationId, players: game.players});
        });

    }
})();

(function() {
    angular.module('app').component('dragboard', {
        templateUrl: 'views/components/dragboard.html',
        controller: ['gameController', 'ngDialog', '$state', 'socket', 'gameApi', '$scope', '$timeout', 'toaster', ctrl],
        controllerAs: 'vm'
    });

    function ctrl(gameController, ngDialog, $state, socket, gameApi, $scope, $timeout, toaster) {
        var vm = this;
        vm.currentRound = gameController.currentRound;
        vm.game = gameController;
        vm.currentRoundRules = vm.game.rounds[vm.currentRound];

        vm.$onInit = function() {
            vm.locationId = gameController.locationId;
            vm.gameId = gameController.gameId;
            vm.game.players.sort(function (a, b) {return b.totalScore - a.totalScore;});
            vm.game.players =  vm.game.players.filter(function( obj ) {
                return obj.playerClear !== true;
            });
            vm.game.players = vm.game.players.slice(0,12);
            if (vm.locationId == 0 || vm.gameId == 0) {
                toaster.pop('danger', "Location", "Going Home Location Not Set");
                $state.go("home");
            } else {
                socket.emit('change room', vm.locationId);
            }
        };

        vm.getSum = function(myData){
            if(typeof myData === 'undefined'){
                return 0;
            }
            return Object.values(myData).reduce(function(sum, value) {
                return sum + parseInt(value);
            }, 0);
        };


    }
})();

(function() {
    angular.module('app').component('game', {
        templateUrl: 'views/components/game.html',
        controller: ['gameController', 'ngDialog', '$state', 'socket', 'gameApi', '$scope', '$timeout', ctrl],
        controllerAs: 'vm'
    });

    function ctrl(gameController, ngDialog, $state, socket, gameApi, $scope, $timeout) {
        var vm = this;
        vm.currentRound = 0;
        vm.game = gameController;
        vm.roundScore = "";
        vm.refreshTop5 = false;
        vm.currentRoundRules = vm.game.rounds[vm.currentRound];
        vm.newPlayer ={firstName: "",lastName:"",score:""};
        vm.$onInit = function() {
            vm.locationId = gameController.locationId;
            vm.gameId = gameController.gameId;
            console.log(gameController);
            if (vm.locationId == 0 || vm.gameId == 0) {
                $state.go("home");
            } else {
                socket.emit('change room', vm.locationId);
            }

            socket.on("new.score", function (data) {
                gameController.players.push({
                    name: data.firstName + " " + data.lastName,
                    firstName: data.firstName,
                    lastName: data.lastName,
                    score: parseInt(data.score),
                    key: data.uniqueKey,
                    show:true
                });
            });
        };
        $scope.$on('$destroy', function (event) {
            socket.removeAllListeners();
            console.log("destroyed");
        });

        vm.addScorePopUp = function() {
            vm.refreshTop5 = true;
           dialog = ngDialog.open({
                template: '/views/popups/newScore.html',
                scope: $scope,
                showClose: false
            });
            dialog.closePromise.then(function (data) {
                vm.newPlayer ={firstName: "",lastName:"",score:""};
                vm.refreshTop5 = false;
            });
        };

        vm.addScore = function() {
            if(vm.newPlayer.firstName !== "" && vm.newPlayer.lastName!=="") {
                gameApi.submitScore(vm.gameId, vm.locationId, vm.newPlayer.firstName, vm.newPlayer.lastName, vm.newPlayer.score, vm.game.randomString(32))
                    .then(function (response) {
                        ngDialog.close();
                        vm.newPlayer.firstName = "";
                        vm.newPlayer.lastName = "";
                        vm.newPlayer.score = "";
                        updateHeader();
                        vm.refreshTop5 = false;
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }
        };

        vm.addRoundPopup = function(index) {
            gameController.players[index].show = false;
            vm.refreshTop5 = true;
            vm.roundPlayer = gameController.players[index];
            vm.roundPlayer.index = index;
            roundDialog = ngDialog.open({
                template: '/views/popups/roundScore.html',
                scope: $scope,
                showClose: false
            });
            roundDialog.closePromise.then(function (data) {
                delete vm.roundPlayer;
                vm.roundScore = "";
                gameController.players[index].show = true;
                vm.refreshTop5 = false;
            });
        };

        vm.addRound = function (){
            gameApi.updateScore(vm.gameId, vm.locationId, vm.roundScore, vm.roundPlayer.key, vm.roundPlayer.index)
                .then(function(response) {
                    if(vm.roundScore > gameController.players[vm.roundPlayer.index].score){
                        gameController.players[vm.roundPlayer.index].score = vm.roundScore;
                    }
                    gameController.players[vm.roundPlayer.index].show =true;
                    ngDialog.close();
                    vm.roundScore = "";
                    vm.roundPlayer = {};
                    updateHeader();
                    vm.refreshTop5 = false;
                })
                .catch(function(error) {
                    console.log(error);
                });
        };

        vm.increasePlayerLimit = function() {
            vm.refreshTop5 = true;
            if (vm.currentRoundRules.playerLimit + 1 > vm.game.totalLanes) {
                alert("not enough lanes");
            } else {
                vm.currentRoundRules.playerLimit++;
                updateHeader();
            }
            vm.refreshTop5 = false;
        };


        var updateHeader = function (){
            gameController.topPlayers = gameController.players.slice(0).sort(function (a, b) {return b.score - a.score;}).slice(0,vm.currentRoundRules.playerLimit);
            var count = gameController.topPlayers.length;
            if(count == vm.currentRoundRules.playerLimit){
                gameController.scoreToBeat = gameController.topPlayers[count-1].score;
            }
        }

       

        /**Clock**/
        vm.increaseClock = function() {
            $scope.$broadcast('timer-add-cd-seconds', 60);
        };

        vm.startClock = function() {
            $scope.$broadcast('timer-start');
            //socket.emit("newScoreToBeat", {room: game.locationId, score: 250});
        };

        vm.pauseClock = function() {
            $scope.$broadcast('timer-stop');
        };
/**
        $scope.$on('timer-tick', function(event, args) {
            if (args.millis % 5000 == 0) {
                socket.emit("updateTime", {
                    room: vm.locationId,
                    mins: args.minutes,
                    seconds: args.seconds
                });
            }
        });
 **/
    }
})();

(function() {
    angular.module('app').component('gameOver', {
        templateUrl: 'views/components/gameOver.html',
        controller: ['gameController','$scope','gameApi', 'toaster', 'ngDialog', '$state', 'socket', ctrl],
        controllerAs: 'vm'
    });

    function ctrl(gameController, $scope, gameApi, toaster, ngDialog, $state, socket) {
        var vm = this;
        vm.players = gameController.players;
        vw.game = gameController;
        vm.currentRoundRules = gameController.rounds[gameController.currentRound];
        vm.$onInit = function() {
            vm.winner = {};
            vm.winner.name ="Pat";
        }


    }
})();

//TODO MAKE TOPPLAYERS TO PLAYER
(function() {
    angular.module('app').component('grid', {
        templateUrl: 'views/components/grid.html',
        controller: ['gameController','$scope','gameApi', 'toaster', 'ngDialog', '$state', 'socket', ctrl],
        controllerAs: 'vm'
    });

    function ctrl(gameController, $scope, gameApi, toaster, ngDialog, $state, socket) {
        var vm = this;
        vm.game = gameController;
        vm.currentRoundRules = gameController.rounds[gameController.currentRound];
        vm.$onInit = function() {
            gameController.players = [];
            vm.roundScore = "";
            vm.locationId = gameController.locationId;
            vm.gameType = gameController.gameTypeId;
            vm.gameId = gameController.gameId;

            if (vm.locationId == 0 || vm.gameId == 0) {
                toaster.pop('danger', "Location", "Going Home Location Not Set");
                $state.go("home");
            }

            var lanes = Array.from(new Array(gameController.totalLanes).keys());
            gameController.topPlayers.forEach(function (s) {
                s.lane = lanes.splice(Math.floor(Math.random() * lanes.length), 1)[0] + 1;
                s.games = new Array(gameController.rounds[gameController.currentRound].totalGames).fill(null).map(function (e) {return {score: null}});
                s.currentRound = 0;
                s.totalScore = 0;
            });
            gameController.topPlayers.sort(function (a, b) {return a.lane - b.lane;});
        };
        vm.addRoundPopup = function(index, gameNumber) {
            console.log(typeof gameNumber == 'undefined');
            if(typeof gameNumber == 'undefined'){
                gameNumber = -1;
            }
            vm.roundPlayer = gameController.topPlayers[index];
            vm.roundPlayer.index = index;
            vm.roundPlayer.updateRound = gameNumber;
            roundDialog = ngDialog.open({
                template: '/views/popups/roundScore.html',
                scope: $scope,
                showClose: false
            });
            roundDialog.closePromise.then(function (data) {
                delete vm.roundPlayer;
                vm.roundScore = "";
            });
        };

        vm.addRound = function (){
            gameApi.updateScore(vm.gameId, vm.locationId, vm.roundScore, vm.roundPlayer.key, vm.roundPlayer.index)
                .then(function(response) {
                    if(vm.roundPlayer.updateRound >= 0){
                        gameController.topPlayers[vm.roundPlayer.index].totalScore -= gameController.topPlayers[vm.roundPlayer.index].games[vm.roundPlayer.updateRound].score;
                        gameController.topPlayers[vm.roundPlayer.index].games[vm.roundPlayer.updateRound].score = vm.roundScore;
                        gameController.topPlayers[vm.roundPlayer.index].totalScore += parseInt(vm.roundScore);
                    } else {
                        gameController.topPlayers[vm.roundPlayer.index].games[vm.roundPlayer.currentRound].score = vm.roundScore;
                        gameController.topPlayers[vm.roundPlayer.index].currentRound++;
                        gameController.topPlayers[vm.roundPlayer.index].totalScore += parseInt(vm.roundScore);
                    }
                    ngDialog.close();
                    vm.roundScore = "";
                    vm.roundPlayer = {};
                })
                .catch(function(error) {
                    console.log(error);
                });
        };


    }
})();

(function() {
    angular.module('app').component('gridView', {
        templateUrl: 'views/components/gridView.html',
        controller: ['gameController', '$state', 'socket', 'toaster', ctrl],
        controllerAs: 'vm'
    });

    function ctrl(gameController, $state, socket, toaster) {
        var vm = this;
        vm.players = gameController.players;
        socket.on("updatePlayers", function(data) {
            vm.players = data.players;
        });
    }

})();

(function() {
    angular.module('app').component('mainMenu', {
        templateUrl: 'views/components/main.html',
        controller: ['gameController','gameApi','socket','$state','locationApi', '$scope', 'ngDialog', ctrl],
        controllerAs: 'vm'
    });

    function ctrl(gameController, gameApi, socket, $state, locationApi, $scope, ngDialog) {
        var vm = this;
          
        vm.$onInit = function() {
            locationApi.getLocations()
                .then(function(response) {
                    vm.locations = response.data;
                })
                .catch(function(error) {
                    console.log(error);
                });
        };
        vm.displayPopup = function() {
            ngDialog.open({
                template: '/views/popups/displayMode.html',
                scope: $scope,
                showClose: false
            });
        };

        vm.newGamePopup = function() {
            ngDialog.open({
                template: '/views/popups/newGame.html',
                scope: $scope,
                showClose: false
            });
        };
        vm.goToDisplay = function() {
            ngDialog.close();
            gameController.locationId = vm.currentLocation;
            $state.go("scoreboard");
            socket.emit('change room', vm.currentLocation);
        };

        vm.createNewGame = function (){
            ngDialog.close();
            gameApi.createGame(vm.newGameLocation, vm.newGameType).then(function (response) {
                gameController.locationId = response.data.locationId;
                gameController.gameId = response.data.id;
                Object.assign(gameController,response.data);
                $state.go(gameController.rounds[0]['type']);
            })
            .catch(function (error) {
                console.log(error);
            });

        }
    };
})();

(function () {
    angular.module('app').factory('gameController', gameController);
    gameController.$inject = ['socket', '$state'];

    function gameController(socket, $state) {
        var game = {};
        game.init = function () {
            game.players = [];
            game.resetState();
        };

        game.resetState = function () {
            game.players = [];
            game.currentRound = 0;
            game.locationId = 0;
            game.gameId = 0;
            game.secret = '';
            game.rounds = [{type:'',totalGames:'',moveAhead:''}];
        };


        game.setUpRound = function (type) {

        };

        game.forceReconnect = function(){
            console.log("moving to "+ game.locationId);
            socket.emit('change room', game.locationId);
        };

        socket.on('reconnect', function() {
            socket.emit('change room', game.locationId);
        });

        socket.on('disconnect', function(){
            game.forceReconnect();
            console.log("i had to reconnect")
        });

        /*
         game.setUpRound = function (type) {
         //if current round is end end the game
         //TODO - End Game
         var lanes = Array.from(new Array(game.totalLanes).keys());
         game.players.forEach(function (s) {
         s.lane = lanes.splice(Math.floor(Math.random() * lanes.length), 1)[0] + 1;
         s.games = new Array(game.rounds[game.currentRound].totalGames).fill(null).map(function (e) {return {score: null}});
         s.currentRound = 0;
         s.totalScore = 0;
         });
         game.players.sort(function (a, b) {return a.lane - b.lane;});

         };
         */

        game.randomString = function(length) {
            var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
            length = length || 10;
            var string = '', rnd;
            while (length > 0) {
                rnd = Math.floor(Math.random() * chars.length);
                string += chars.charAt(rnd);
                length--;
            }
            return string;
        };
        game.moveToNextRound = function(){
            if (confirm("Are you sure you want to end this round?")){
                // play on
                game.currentRound++;
                round = game.rounds[game.currentRound].type;
                if (game.currentRound == game.totalRounds) {
                    socket.emit("scoreboardChange", {room: game.locationId, type: "gameOver"});
                    $state.go("gameOver");
                } else {
                    this.setUpRound();
                    socket.emit("scoreboardChange", {room: game.locationId, type: "gridView"});
                    $state.go(round);
                }
            }
        };
        game.init();
        return game;
    }
})();

(function() {
    angular.module('app').component('scoreboard', {
        templateUrl: 'views/components/scoreboard.html',
        controller: ['gameController', '$state', 'socket', 'toaster', ctrl],
        controllerAs: 'vm'
    });

    function ctrl(gameController, $state, socket, toaster) {
        var vm = this;
        vm.$onInit = function() {
            vm.locationId = gameController.locationId;
            if (vm.locationId == 0) {
                toaster.pop('danger', "Location", "Going Home Location Not Set");
                $state.go("home");
            }
        };
        vm.timer = '0:00';
        vm.players = [];
        vm.scoreToBeat = 250;
        vm.maxLimit = 8;
        vm.started = false;

        socket.on("updateLowest", function(data) {
            vm.scoreToBeat = data;
        });

        socket.on("updateTime", function(data) {
            vm.started = true;
            vm.timer = data.mins + ':' + data.seconds;
        });

        socket.on("timeStarted", function(data) {
            vm.started = true;
        });

        socket.on("changeView", function(data) {
            $state.go(data);
        });

    }
})();
