(function() {
    angular.module('app').component('gridView', {
        templateUrl: 'views/components/gridView.html',
        controller: ['gameController', '$state', 'socket', 'toaster', ctrl],
        controllerAs: 'vm'
    });

    function ctrl(gameController, $state, socket, toaster) {
        var vm = this;
        vm.players = gameController.players;
        socket.on("updatePlayers", function(data) {
            vm.players = data.players;
        });
    }

})();
