(function() {
    angular.module('app').component('game', {
        templateUrl: 'views/components/game.html',
        controller: ['gameController', 'ngDialog', '$state', 'socket', 'gameApi', '$scope', '$timeout', ctrl],
        controllerAs: 'vm'
    });

    function ctrl(gameController, ngDialog, $state, socket, gameApi, $scope, $timeout) {
        var vm = this;
        vm.currentRound = 0;
        vm.game = gameController;
        vm.roundScore = "";
        vm.refreshTop5 = false;
        vm.currentRoundRules = vm.game.rounds[vm.currentRound];
        vm.newPlayer ={firstName: "",lastName:"",score:""};
        vm.$onInit = function() {
            vm.locationId = gameController.locationId;
            vm.gameId = gameController.gameId;
            console.log(gameController);
            if (vm.locationId == 0 || vm.gameId == 0) {
                $state.go("home");
            } else {
                socket.emit('change room', vm.locationId);
            }

            socket.on("new.score", function (data) {
                gameController.players.push({
                    name: data.firstName + " " + data.lastName,
                    firstName: data.firstName,
                    lastName: data.lastName,
                    score: parseInt(data.score),
                    key: data.uniqueKey,
                    show:true
                });
            });
        };
        $scope.$on('$destroy', function (event) {
            socket.removeAllListeners();
            console.log("destroyed");
        });

        vm.addScorePopUp = function() {
            vm.refreshTop5 = true;
           dialog = ngDialog.open({
                template: '/views/popups/newScore.html',
                scope: $scope,
                showClose: false
            });
            dialog.closePromise.then(function (data) {
                vm.newPlayer ={firstName: "",lastName:"",score:""};
                vm.refreshTop5 = false;
            });
        };

        vm.addScore = function() {
            if(vm.newPlayer.firstName !== "" && vm.newPlayer.lastName!=="") {
                gameApi.submitScore(vm.gameId, vm.locationId, vm.newPlayer.firstName, vm.newPlayer.lastName, vm.newPlayer.score, vm.game.randomString(32))
                    .then(function (response) {
                        ngDialog.close();
                        vm.newPlayer.firstName = "";
                        vm.newPlayer.lastName = "";
                        vm.newPlayer.score = "";
                        updateHeader();
                        vm.refreshTop5 = false;
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }
        };

        vm.addRoundPopup = function(index) {
            gameController.players[index].show = false;
            vm.refreshTop5 = true;
            vm.roundPlayer = gameController.players[index];
            vm.roundPlayer.index = index;
            roundDialog = ngDialog.open({
                template: '/views/popups/roundScore.html',
                scope: $scope,
                showClose: false
            });
            roundDialog.closePromise.then(function (data) {
                delete vm.roundPlayer;
                vm.roundScore = "";
                gameController.players[index].show = true;
                vm.refreshTop5 = false;
            });
        };

        vm.addRound = function (){
            gameApi.updateScore(vm.gameId, vm.locationId, vm.roundScore, vm.roundPlayer.key, vm.roundPlayer.index)
                .then(function(response) {
                    if(vm.roundScore > gameController.players[vm.roundPlayer.index].score){
                        gameController.players[vm.roundPlayer.index].score = vm.roundScore;
                    }
                    gameController.players[vm.roundPlayer.index].show =true;
                    ngDialog.close();
                    vm.roundScore = "";
                    vm.roundPlayer = {};
                    updateHeader();
                    vm.refreshTop5 = false;
                })
                .catch(function(error) {
                    console.log(error);
                });
        };

        vm.increasePlayerLimit = function() {
            vm.refreshTop5 = true;
            if (vm.currentRoundRules.playerLimit + 1 > vm.game.totalLanes) {
                alert("not enough lanes");
            } else {
                vm.currentRoundRules.playerLimit++;
                updateHeader();
            }
            vm.refreshTop5 = false;
        };


        var updateHeader = function (){
            gameController.topPlayers = gameController.players.slice(0).sort(function (a, b) {return b.score - a.score;}).slice(0,vm.currentRoundRules.playerLimit);
            var count = gameController.topPlayers.length;
            if(count == vm.currentRoundRules.playerLimit){
                gameController.scoreToBeat = gameController.topPlayers[count-1].score;
            }
        }

       

        /**Clock**/
        vm.increaseClock = function() {
            $scope.$broadcast('timer-add-cd-seconds', 60);
        };

        vm.startClock = function() {
            $scope.$broadcast('timer-start');
            //socket.emit("newScoreToBeat", {room: game.locationId, score: 250});
        };

        vm.pauseClock = function() {
            $scope.$broadcast('timer-stop');
        };
/**
        $scope.$on('timer-tick', function(event, args) {
            if (args.millis % 5000 == 0) {
                socket.emit("updateTime", {
                    room: vm.locationId,
                    mins: args.minutes,
                    seconds: args.seconds
                });
            }
        });
 **/
    }
})();
