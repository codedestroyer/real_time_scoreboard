
angular
.module('app', [
  'btford.socket-io',
  'bc.AngularKeypad',
  'ui.router',
  'ngSanitize',
  'ngAnimate',
  'timer',
  'toaster',
  'ngDialog',
  'dndLists'
]);
angular.module('app').
factory('socket', function (socketFactory) {
  return socketFactory({ioSocket: io.connect('local.skeeball.com:6001/',{
    reconnection: true,
    reconnectionDelay: 1000,
    reconnectionDelayMax : 5000,
    reconnectionAttempts: Infinity
  })});
}).
value('version', '0.9');
