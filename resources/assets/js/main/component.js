(function() {
    angular.module('app').component('mainMenu', {
        templateUrl: 'views/components/main.html',
        controller: ['gameController','gameApi','socket','$state','locationApi', '$scope', 'ngDialog', ctrl],
        controllerAs: 'vm'
    });

    function ctrl(gameController, gameApi, socket, $state, locationApi, $scope, ngDialog) {
        var vm = this;
          
        vm.$onInit = function() {
            locationApi.getLocations()
                .then(function(response) {
                    vm.locations = response.data;
                })
                .catch(function(error) {
                    console.log(error);
                });
        };
        vm.displayPopup = function() {
            ngDialog.open({
                template: '/views/popups/displayMode.html',
                scope: $scope,
                showClose: false
            });
        };

        vm.newGamePopup = function() {
            ngDialog.open({
                template: '/views/popups/newGame.html',
                scope: $scope,
                showClose: false
            });
        };
        vm.goToDisplay = function() {
            ngDialog.close();
            gameController.locationId = vm.currentLocation;
            $state.go("scoreboard");
            socket.emit('change room', vm.currentLocation);
        };

        vm.createNewGame = function (){
            ngDialog.close();
            gameApi.createGame(vm.newGameLocation, vm.newGameType).then(function (response) {
                gameController.locationId = response.data.locationId;
                gameController.gameId = response.data.id;
                Object.assign(gameController,response.data);
                $state.go(gameController.rounds[0]['type']);
            })
            .catch(function (error) {
                console.log(error);
            });

        }
    };
})();
