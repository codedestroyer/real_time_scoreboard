(function() {
    angular.module('app').component('dragboard', {
        templateUrl: 'views/components/dragboard.html',
        controller: ['gameController', 'ngDialog', '$state', 'socket', 'gameApi', '$scope', '$timeout', 'toaster', ctrl],
        controllerAs: 'vm'
    });

    function ctrl(gameController, ngDialog, $state, socket, gameApi, $scope, $timeout, toaster) {
        var vm = this;
        vm.currentRound = gameController.currentRound;
        vm.game = gameController;
        vm.currentRoundRules = vm.game.rounds[vm.currentRound];

        vm.$onInit = function() {
            vm.locationId = gameController.locationId;
            vm.gameId = gameController.gameId;
            vm.game.players.sort(function (a, b) {return b.totalScore - a.totalScore;});
            vm.game.players =  vm.game.players.filter(function( obj ) {
                return obj.playerClear !== true;
            });
            vm.game.players = vm.game.players.slice(0,12);
            if (vm.locationId == 0 || vm.gameId == 0) {
                toaster.pop('danger', "Location", "Going Home Location Not Set");
                $state.go("home");
            } else {
                socket.emit('change room', vm.locationId);
            }
        };

        vm.getSum = function(myData){
            if(typeof myData === 'undefined'){
                return 0;
            }
            return Object.values(myData).reduce(function(sum, value) {
                return sum + parseInt(value);
            }, 0);
        };


    }
})();
