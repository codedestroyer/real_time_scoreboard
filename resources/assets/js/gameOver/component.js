(function() {
    angular.module('app').component('gameOver', {
        templateUrl: 'views/components/gameOver.html',
        controller: ['gameController','$scope','gameApi', 'toaster', 'ngDialog', '$state', 'socket', ctrl],
        controllerAs: 'vm'
    });

    function ctrl(gameController, $scope, gameApi, toaster, ngDialog, $state, socket) {
        var vm = this;
        vm.players = gameController.players;
        vw.game = gameController;
        vm.currentRoundRules = gameController.rounds[gameController.currentRound];
        vm.$onInit = function() {
            vm.winner = {};
            vm.winner.name ="Pat";
        }


    }
})();
