(function () {
    angular.module('app').factory('gameApi', gameApi);
    gameApi.$inject = ['$http'];
    function gameApi($http) {
        return {
            submitScore: function (gameId,location,first, last, score, key) {
                return $http.post('/api/game/score',
                     {
                            gameId: gameId,
                            location:location,
                            firstName: first,
                            lastName: last,
                            score: score,
                            key: key
                            }
                );
            },
            submitRound: function (gameId,location,first, last, score, key) {
                return $http.post('/api/game/round',
                    {
                        gameId: gameId,
                        location:location,
                        firstName: first,
                        lastName: last,
                        score: score,
                        key: key}
                );
            },
                submitTotal: function (gameId,location, score, key, index) {
                    return $http.post('/api/game/addTotal',
                        {
                            gameId: gameId,
                            location:location,
                            score: score,
                            index: index,
                            key: key}
                    );
                },
            updateScore: function (gameId,location, score, key, index) {
                return $http.patch('/api/game/score',
                     {

                         gameId: gameId,
                         location:location,
                         score: score,
                         index: index,
                         key: key}
                );
            },
            createGame: function (location, gameType) {
                return $http.post('/api/game/',
                     {
                            location:location,
                            gameType: gameType}
                );
            },
            getGames: function (key,gameId){
                return $http.get("api/game/list",{params: { key: key, gameId:gameId}});
            }
        };
    }
})();
