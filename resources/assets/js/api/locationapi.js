(function () {
    angular.module('app').factory('locationApi', locationApi);
    locationApi.$inject = ['$http'];
    function locationApi($http) {
        return {
            getLocations: function () {
                return $http.get('/api/location');
            }
        };
    }
})();
