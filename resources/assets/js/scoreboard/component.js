(function() {
    angular.module('app').component('scoreboard', {
        templateUrl: 'views/components/scoreboard.html',
        controller: ['gameController', '$state', 'socket', 'toaster', ctrl],
        controllerAs: 'vm'
    });

    function ctrl(gameController, $state, socket, toaster) {
        var vm = this;
        vm.$onInit = function() {
            vm.locationId = gameController.locationId;
            if (vm.locationId == 0) {
                toaster.pop('danger', "Location", "Going Home Location Not Set");
                $state.go("home");
            }
        };
        vm.timer = '0:00';
        vm.players = [];
        vm.scoreToBeat = 250;
        vm.maxLimit = 8;
        vm.started = false;

        socket.on("updateLowest", function(data) {
            vm.scoreToBeat = data;
        });

        socket.on("updateTime", function(data) {
            vm.started = true;
            vm.timer = data.mins + ':' + data.seconds;
        });

        socket.on("timeStarted", function(data) {
            vm.started = true;
        });

        socket.on("changeView", function(data) {
            $state.go(data);
        });

    }
})();
