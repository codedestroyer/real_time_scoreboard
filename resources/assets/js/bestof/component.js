(function() {
    angular.module('app').component('bestof', {
        templateUrl: 'views/components/bestof.html',
        controller: ['gameController', 'ngDialog', '$state', 'socket', 'gameApi', '$scope', '$timeout', ctrl],
        controllerAs: 'vm'
    });

    function ctrl(gameController, ngDialog, $state, socket, gameApi, $scope, $timeout) {
        var vm = this;
        vm.currentRound = 0;
        vm.game = gameController;
        vm.roundScore = "";
        vm.currentRoundRules = vm.game.rounds[vm.currentRound];
        vm.newPlayer ={firstName: "",lastName:"",score:""};
        vm.$onInit = function() {
            vm.locationId = gameController.locationId;
            vm.gameId = gameController.gameId;
            if (vm.locationId == 0 || vm.gameId == 0) {
                $state.go("home");
            } else {
                socket.emit('change room', vm.locationId);
            }
        };
        
        $scope.$on('$destroy', function (event) {
            socket.removeAllListeners();
            console.log("destroyed");
        });

        vm.addScorePopUp = function() {
            ngDialog.open({
                template: '/views/popups/newScore.html',
                scope: $scope,
                showClose: false
            });
        };

        vm.addScore = function() {
            if(vm.newPlayer.firstName !== "" && vm.newPlayer.lastName!=="") {
                gameApi.submitScore(vm.gameId, vm.locationId, vm.newPlayer.firstName, vm.newPlayer.lastName, vm.newPlayer.score, vm.game.randomString(32))
                    .then(function (response) {
                        ngDialog.close();
                        vm.newPlayer.firstName = "";
                        vm.newPlayer.lastName = "";
                        vm.newPlayer.score = "";
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }
        };

        /** Add Game to PLayer **/
        vm.addRoundPopup = function(index) {
            gameController.players[index].show = false;
            vm.roundPlayer = gameController.players[index];
            vm.roundPlayer.index = index;
            roundDialog = ngDialog.open({
                template: '/views/popups/roundScore.html',
                scope: $scope,
                showClose: false
            });
            roundDialog.closePromise.then(function (data) {
                delete vm.roundPlayer;
                vm.roundScore = "";
                $timeout(function() {
                    gameController.players[index].show = true;
                }, 500);
            });
        };

        vm.addRound = function (){
            gameApi.submitTotal(vm.gameId, vm.locationId, vm.roundScore, vm.roundPlayer.key, vm.roundPlayer.index)
                .then(function(response) {
                    ngDialog.close();
                    vm.roundScore = "";
                    vm.roundPlayer = {};
                })
                .catch(function(error) {
                    console.log(error);
                });
        };
        
        
        vm.removePlayer = function (index){
            if(gameController.players[index].playerClear){
                deletePlayer = confirm("Are you sure you want to reactivate user?");
                if(deletePlayer){
                    gameController.players[index].playerClear = false;
                }
            } else {
                deletePlayer = confirm("Are you sure you want to Inactive user?");
                if(deletePlayer){
                    gameController.players[index].playerClear = true;
                }
            }


        };


        //TODO UPDATE
        vm.viewGames = function (index){
            vm.roundPlayer = gameController.players[index];
            gameApi.getGames(vm.roundPlayer.key,vm.gameId).then(function(response){
                vm.tempScore = response.data;
                dialog = ngDialog.open({
                    template: '/views/popups/viewScores.html',
                    scope: $scope,
                    showClose: false
                });
                dialog.closePromise.then(function (data) {
                    delete vm.roundPlayer;
                    delete vm.tempScore;
                });
            });
        };



        /** Socket Events **/
        socket.on("new.score", function (data) {
                gameController.players.push({
                    name: data.firstName + " " + data.lastName,
                    firstName: data.firstName,
                    lastName: data.lastName,
                    score: parseInt(data.score),
                    totalScore: parseInt(data.score),
                    lowestScore: parseInt(data.score),
                    key: data.uniqueKey,
                    show:true,
                    playerClear:false,
                    gamesPlayed: 1
                });
        });

        //during elimination
        socket.on('round.score',function(data){
            console.log("new score");
            gameController.players[data.index].totalScore = data.totalScore;
            gameController.players[data.index].lowestScore = data.lowestScore;
            gameController.players[data.index].gamesPlayed++;
            gameController.players[data.index].show =true;
           // socket.emit("updatePlayers", {room: game.locationId, players: game.players});
        });

    }
})();
