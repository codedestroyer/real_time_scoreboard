/**
 * Created by codedestroyer on 5/8/17.
 */
angular
    .module('app')
    .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'views/main.html'
            }).state('entry', {
                url: '/game',
                templateUrl: 'views/game.html'
            }).state('bestOf', {
            url: '/bestof',
            templateUrl: 'views/bestOf.html'
        }).state('dragboard', {
            url: '/drag',
            templateUrl: 'views/dragboard.html'
        }).state('elimination', {
                url: '/grid',
                templateUrl: 'views/grid.html'
            }).state('scoreboard', {
            url: '/scoreboard',
            templateUrl: 'views/scoreboard.html'

        }).state('gridView', {
        url: '/gridView',
        templateUrl: 'views/gridView.html'

    }).state('gameOver', {
            url: '/gameOver',
            templateUrl: 'views/gameOver.html'

        });
        angular.module('app').run(['$http', '$urlRouter', function ($http, runtimeStates, $urlRouter) {

        }]);
    }]);
