//TODO MAKE TOPPLAYERS TO PLAYER
(function() {
    angular.module('app').component('grid', {
        templateUrl: 'views/components/grid.html',
        controller: ['gameController','$scope','gameApi', 'toaster', 'ngDialog', '$state', 'socket', ctrl],
        controllerAs: 'vm'
    });

    function ctrl(gameController, $scope, gameApi, toaster, ngDialog, $state, socket) {
        var vm = this;
        vm.game = gameController;
        vm.currentRoundRules = gameController.rounds[gameController.currentRound];
        vm.$onInit = function() {
            gameController.players = [];
            vm.roundScore = "";
            vm.locationId = gameController.locationId;
            vm.gameType = gameController.gameTypeId;
            vm.gameId = gameController.gameId;

            if (vm.locationId == 0 || vm.gameId == 0) {
                toaster.pop('danger', "Location", "Going Home Location Not Set");
                $state.go("home");
            }

            var lanes = Array.from(new Array(gameController.totalLanes).keys());
            gameController.topPlayers.forEach(function (s) {
                s.lane = lanes.splice(Math.floor(Math.random() * lanes.length), 1)[0] + 1;
                s.games = new Array(gameController.rounds[gameController.currentRound].totalGames).fill(null).map(function (e) {return {score: null}});
                s.currentRound = 0;
                s.totalScore = 0;
            });
            gameController.topPlayers.sort(function (a, b) {return a.lane - b.lane;});
        };
        vm.addRoundPopup = function(index, gameNumber) {
            console.log(typeof gameNumber == 'undefined');
            if(typeof gameNumber == 'undefined'){
                gameNumber = -1;
            }
            vm.roundPlayer = gameController.topPlayers[index];
            vm.roundPlayer.index = index;
            vm.roundPlayer.updateRound = gameNumber;
            roundDialog = ngDialog.open({
                template: '/views/popups/roundScore.html',
                scope: $scope,
                showClose: false
            });
            roundDialog.closePromise.then(function (data) {
                delete vm.roundPlayer;
                vm.roundScore = "";
            });
        };

        vm.addRound = function (){
            gameApi.updateScore(vm.gameId, vm.locationId, vm.roundScore, vm.roundPlayer.key, vm.roundPlayer.index)
                .then(function(response) {
                    if(vm.roundPlayer.updateRound >= 0){
                        gameController.topPlayers[vm.roundPlayer.index].totalScore -= gameController.topPlayers[vm.roundPlayer.index].games[vm.roundPlayer.updateRound].score;
                        gameController.topPlayers[vm.roundPlayer.index].games[vm.roundPlayer.updateRound].score = vm.roundScore;
                        gameController.topPlayers[vm.roundPlayer.index].totalScore += parseInt(vm.roundScore);
                    } else {
                        gameController.topPlayers[vm.roundPlayer.index].games[vm.roundPlayer.currentRound].score = vm.roundScore;
                        gameController.topPlayers[vm.roundPlayer.index].currentRound++;
                        gameController.topPlayers[vm.roundPlayer.index].totalScore += parseInt(vm.roundScore);
                    }
                    ngDialog.close();
                    vm.roundScore = "";
                    vm.roundPlayer = {};
                })
                .catch(function(error) {
                    console.log(error);
                });
        };


    }
})();
