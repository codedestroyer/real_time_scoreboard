(function () {
    angular.module('app').factory('gameController', gameController);
    gameController.$inject = ['socket', '$state'];

    function gameController(socket, $state) {
        var game = {};
        game.init = function () {
            game.players = [];
            game.resetState();
        };

        game.resetState = function () {
            game.players = [];
            game.currentRound = 0;
            game.locationId = 0;
            game.gameId = 0;
            game.secret = '';
            game.rounds = [{type:'',totalGames:'',moveAhead:''}];
        };


        game.setUpRound = function (type) {

        };

        game.forceReconnect = function(){
            console.log("moving to "+ game.locationId);
            socket.emit('change room', game.locationId);
        };

        socket.on('reconnect', function() {
            socket.emit('change room', game.locationId);
        });

        socket.on('disconnect', function(){
            game.forceReconnect();
            console.log("i had to reconnect")
        });

        /*
         game.setUpRound = function (type) {
         //if current round is end end the game
         //TODO - End Game
         var lanes = Array.from(new Array(game.totalLanes).keys());
         game.players.forEach(function (s) {
         s.lane = lanes.splice(Math.floor(Math.random() * lanes.length), 1)[0] + 1;
         s.games = new Array(game.rounds[game.currentRound].totalGames).fill(null).map(function (e) {return {score: null}});
         s.currentRound = 0;
         s.totalScore = 0;
         });
         game.players.sort(function (a, b) {return a.lane - b.lane;});

         };
         */

        game.randomString = function(length) {
            var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
            length = length || 10;
            var string = '', rnd;
            while (length > 0) {
                rnd = Math.floor(Math.random() * chars.length);
                string += chars.charAt(rnd);
                length--;
            }
            return string;
        };
        game.moveToNextRound = function(){
            if (confirm("Are you sure you want to end this round?")){
                // play on
                game.currentRound++;
                round = game.rounds[game.currentRound].type;
                if (game.currentRound == game.totalRounds) {
                    socket.emit("scoreboardChange", {room: game.locationId, type: "gameOver"});
                    $state.go("gameOver");
                } else {
                    this.setUpRound();
                    socket.emit("scoreboardChange", {room: game.locationId, type: "gridView"});
                    $state.go(round);
                }
            }
        };
        game.init();
        return game;
    }
})();
