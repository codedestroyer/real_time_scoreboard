
<!DOCTYPE html>
<html lang="en" ng-app="app">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Prime - Bootstrap 4 Admin Template</title>
    <!-- Main styles for this application -->
    <link href="css/style.css" rel="stylesheet">
    <link href="bower_components/angular-toastr/dist/angular-toastr.css" rel="stylesheet">
    <link rel="stylesheet" href="bower_components/ng-dialog/css/ngDialog.min.css">
    <link rel="stylesheet" href="bower_components/ng-dialog/css/ngDialog-theme-default.min.css">
    <script src="//local.skeeball.com:6001/socket.io/socket.io.js"></script>
</head>

<body>
<body class="app">

<div class="app-body">
    <main class="main">
        <div class="container-fluid">
            <div id="ui-view" ui-view="" style="opacity: 1;">
            </div>
        </div>
    </main>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-body">
                <div class="form-group">
                    <label for="company">Location</label>
                    <input type="text" class="form-control" id="company" placeholder="Enter your company name">
                </div>
                <div class="form-group">
                    <label for="company">Game Type</label>
                    <input type="text" class="form-control" id="company" placeholder="Enter your company name">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<toaster-container toaster-options="{'time-out': 3000}"></toaster-container>
</body>
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="bower_components/tether/dist/js/tether.min.js"></script>
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- AngularJS -->
<script src="bower_components/angular/angular.min.js"></script>

<!-- AngularJS plugins -->
<script src="bower_components/angular-animate/angular-animate.min.js"></script>
<script src="bower_components/angular-sanitize/angular-sanitize.min.js"></script>
<script src="bower_components/AngularJS-Toaster/toaster.js"></script>
<script src="bower_components/angular-ui-router/release/angular-ui-router.min.js"></script>
<script src="bower_components/oclazyload/dist/ocLazyLoad.min.js"></script>
<script src="bower_components/angular-translate/angular-translate.min.js"></script>
<script src="bower_components/angular-breadcrumb/dist/angular-breadcrumb.min.js"></script>
<script src="bower_components/angular-loading-bar/build/loading-bar.min.js"></script>
<script src="bower_components/angular-socket-io/socket.js"></script>
<script src="bower_components/angular-timer/dist/angular-timer.js"></script>
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/moment/min/locales.min.js"></script>
<script src="bower_components/ng-dialog/js/ngDialog.min.js"></script>
<script src="bower_components/humanize-duration/humanize-duration.js"></script>
<script src="bower_components//angular-keypad/dist/angular-keypad.js"></script>
<link rel="stylesheet" href="bower_components/angular-keypad/dist/angular-keypad.css">




<!-- AngularJS Prime App scripts -->

<script src="js/app.js"></script>
</body>
</html>
